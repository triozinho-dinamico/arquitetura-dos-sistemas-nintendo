<h1> Repositório do Grupo - Disciplina SSC0510 </h1>

Alunos: 
- Geraldo Murilo Carrijo Viana Alves da Silva (11849306) - murilocarrijosilva@usp.br
- Lucas Caetano Procópio (11831338) - lucascbsi020@usp.br
- Iara Duarte Mainates (11816143) - iaramainates@usp.br

<h2> Prova - 11/01/2022 </h2>
- Disponível neste gitlab, na pasta "PROVA"


<h2> Trabalho - SUN Sparc</h2>

- Vídeo: Disponível via Google Drive (https://drive.google.com/file/d/1kbb71EvENWxfxhh5uTWHcAgdgO4aO_9P/view?usp=sharing)

- Apresentação: Disponível neste gitlab, na pasta "TRABALHO"

- Outras informações e curiosidades: Disponível neste gitlab, no repositório "Outros Arquivos" dentro da pasta "TRABALHO"

<h3> Descrição: Sun Sparc e a importância do Open Source na evolução da arquitetura de computadores </h3>

SPARC (Scalable Processor ARChitecture) é uma arquitetura aberta extremamente influenciada pelo sistema experimental Berkeley RISC. 
Após seu lançamento em 1987, o SPARC foi um dos maiores sucessos comerciais entre os sistemas RISC e seu sucesso levou a adoção dessa mesma arquitetura por várias empresas.

Em 1989 foi criada a SPARC International. Essa organização era responsável por gerenciar as marcas comerciais SPARC e fornecer testes de conformidade. 
Com os objetivos listados anteriormente  a SPARC International licenciou a arquitetura SPARC para que essa se tornasse pública e Roalty-free

<h3> Características </h3>
É uma arquitetura load-store (também conhecida como orientada a registrador): As únicas instruções que acessam memória são as instruções de load e store, o resto opera nos registradores.

Reconhece três formatos para dados:
- Inteiro com sinal (8, 16, 32 e 64 bits);
- Inteiro sem sinal (8, 16, 32 e 64 bits);
- Ponto flutuante (32, 64 e 128 bits).

<h4>Registradores</h4>
O processador SPARC geralmente contém até 160 registradores de uso geral. 
De acordo com a especificação "Oracle SPARC Architecture 2015", uma "implementação pode conter de 72 a 640 registros de 64 bits de uso geral".
- SPARC faz uso das janelas de registradores. 
- Cada janela possui 24 registros, o número de janelas é dependente da implementação e varia de 2 a 32 janelas.  
- Acessar uma janela implica em poder trabalhar com 24 registradores.

<h4>Instruções</h4>
- Todas as instruções ocupam 32 bits e são divididas em 4 formatos que podem ser distintos pelos 2 primeiros bits. 
- Todas as intruções lógicas aritméticas tem dois operandos de origem e um de destino.


<h3>Arquitetura Aberta - Beneficios</h3>
- Aumentar a reputação e relevância de uma tecnologia útil a todos
- Criar um ecossistema colaborativo
- Reduzir custos
- Inovar/Evoluir um projeto e gerar competitividade no mercado
- Democratização do Produto

<h3>Fontes Utilizadas</h3>
Teclado: https://deskthority.net/wiki/Sun_Type_6

Mouse
- https://www.oldmouse.com/mouse/mousesystems/m4.shtml
- https://docs.oracle.com/cd/E23824_01/html/E24676/glnkn.html

SPARCclassic
- http://q7.neurotica.com/Oldtech/Sun/Classic.html
- https://giovannireisnunes.files.wordpress.com/2015/12/sparc-sshot.jpeg
- https://www.vaxbarn.com/index.php/other-bits/247%20-sun-classic
- https://shrubbery.net/~heas/sun-feh-2_1/Systems/SUNERGY/CPU_Classic.html
- http://www.computinghistory.org.uk/det/6751/Sun-SPARCclassic/
- http://eintr.net/systems/sun/sparcclassic/
- https://docs.oracle.com/cd/E19957-01/801-2211-11/
- http://www.sunhelp.org/faq/sunref1.html
- http://fm4dd.com/misc/sparc-classic/
- https://blog.banana.fish/blog/2014-01-20-playing-with-some-old-sparc-boxes/
- https://en.wikipedia.org/wiki/SPARCstation
- https://en.wikipedia.org/wiki/SPARCclassic
- https://en.wikipedia.org/wiki/MicroSPARC

SCSI driver: http://www.computinghistory.org.uk/det/6722/SUN-411-with-a-HP-C1539-dds2-Tape-Drive/

História da Sun: https://www.britannica.com/topic/Sun-Microsystems-Inc